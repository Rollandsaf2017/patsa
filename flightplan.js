//manual
// #  npm run build:prod
// rsync -va dist/* webmaster@106.14.250.39:~/patsa/website
//TODO remote must be git repo

//http://patsa-parsscu.org/

// pm2 start -r dotenv/config -r esm app.js --name cs2020
// pm2 start index.js --name cs2020 ====> for production


var plan = require('flightplan');
var appName = 'africacv';
var appPath = 'africacv/server';
var username = 'webmaster';

// configuration
plan.target('production', [
  {
    host: '106.14.250.39',
    username: username,
    agent: process.env.SSH_AUTH_SOCK
  }
]);
// run commands on localhost
plan.local(function (local) {

  local.exec('git add .');
  local.exec('git commit -m "fly update"');

  var filesToCopy = local.exec('git ls-files', { exec: { maxBuffer: 1000000 * 1024 } });
  // rsync files to all the destination's hosts
  local.transfer(filesToCopy, '~/' + appPath);
});

// run commands on remote hosts (destinations)
plan.remote(function (remote) {


  remote.log('Install dependencies');
  // remote.sudo('cd ~/'+appPath+ ' && npm install --production', {user: username});
  remote.log('Reload application');
  remote.exec('pm2 reload ' + appName, { user: username });
  //
  remote.exec('pm2 logs ' + appName, { user: username });
});
